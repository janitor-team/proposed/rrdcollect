In this directory you can find some usefull examples of rrdcollect
configuration. In each file there are:
 - steps needed to prepare the system for gathering data (if necessary)
 - command to create an RRD archive
 - configuration snippet for rrdcollect.conf.

Please note, that (unless stated otherwise) RRD archives are created for
following timespans:
 - 60 rows of 1 minute average data (data gathered for 1 hour)
 - 72 rows of 1 hour average and maximum data (3 days)
 - 53 rows of 1 week average and maximum (1 year)

Data needs to be fed every 30 seconds with 90 seconds tolerance.

If you need to create RRD archives with different parameters feel free
to use a web tool available at: http://blabluga.hell.pl/rrdwizard/

I'd be glad to accept other examples of usefull configuration.
Please report the bug either using sourceforge tracker:
http://sourceforge.net/tracker/?group_id=61278&atid=496705
or to Debian BTS using reportbug command.
