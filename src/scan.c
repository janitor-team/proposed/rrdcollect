/* scan.c - yet another crude scanf(3) implementation */
/*
 *  RRDcollect  --  Round-Robin Database Collecting Daemon.
 *  Copyright (C) 2002, 2003  Dawid Kuroczko <qnex@knm.org.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "rrdcollect.h"

#ifndef HAVE_STRNDUP
char *strndup(const char *s, size_t n)
{
	char *d;
	size_t i;

	if (!n)
		return NULL;
	
       	d = malloc(n + 1);
	for (i = 0; i < n; i++)
		d[i] = s[i];
	d[i] = '\0';
	return d;
}
#endif /* HAVE_STRNDUP */

#define isoctdigit(a) (a >= '0' && a <= '7')

static char *valuedup(const long int value)
{
	char dec[21];
	snprintf(dec, sizeof(dec), "%ld", value);
	return strdup(dec);
}

/* Function scans buffer for simplified scanf(3) style patterns */

int scan(const char *buf, const char *fmt, struct counter_t **counter)
{
	int i = 0;
	while (*(fmt) != '\0') {
		switch (*fmt) {
			case '%':
				if (*(++fmt) == '%') {
					if (*buf != '%')
						return 1;
					buf++;
				} else {
					int ignore = 0;
					int length = -1;
					long int value = 0;
					const char *start;

					if (*fmt == '*')
						ignore = 1, fmt++;

					if (isdigit(*fmt)) {
						length = atoi(fmt);
						do {
							fmt++;
						} while (isdigit(*fmt));
					}

					/* skip white spaces like scanf does */
					if (strchr("difuoxX", *fmt))
						while (isspace(*buf))
							buf++;
					
					/* FIXME: we should check afterward:
					 * if (start == buf || start == '-' && buf-start == 1)
					 * 	die("WTF???  zero-length number???");
					 */
					start = buf;

					switch (*fmt) {
						/* if it is signed int or float,
						 * we can have minus in front */
						case 'd':
						case 'i':
						case 'f':
							if (*buf == '-' && length)
								buf++, length--;
						case 'u':
							while (isdigit(*buf) && length)
								buf++, length--;
							
							/* integer value ends here */
							if (*fmt == 'f' && *buf == '.' && length) {
								buf++, length--;
								while (isdigit(*buf) && length)
									buf++, length--;
							}


							/* ignore if value not found */
							if((start == buf) || ((atoi(start) == '-') && (buf-start == 1)))
								break;

							if (!ignore)
								counter[i++]->value = strndup(start, buf-start);
							break;

						case 'o':
							while (isoctdigit(*buf) && length) {
								value <<= 3;
								value += *buf - '0';
								buf++, length--;
							}

							/* ignore if value not found */
							if(start == buf)
								break;

							if (!ignore)
								counter[i++]->value = valuedup(value);
							break;
							
						case 'x':
						case 'X':
							while (isxdigit(*buf) && length) {
								value <<= 4;
								if (isdigit(*buf))
									value += *buf - '0';
								else if (islower(*buf))
									value += *buf - 'a' + 10;
								else
									value += *buf - 'A' + 10;
								
								buf++, length--;
							}

							/* ignore if value not found */
							if(start == buf)
								break;
							
							if (!ignore)
								counter[i++]->value = valuedup(value);
							break;

						case 's':
							while (!isspace(*buf) && length) {
								buf++, length--;
							}

							if (!ignore)
								counter[i++]->value = strndup(start, buf - start);
							break;
							
						case 'c':
							if (length < 0)
							     length = 1;	// default length is 1
							
							while (*buf && length > 0) {
								buf++, length--;
							}
							if (length > 0)
								return 2;

							if (!ignore)
								counter[i++]->value = strndup(start, buf - start);
							break;
							
							
						default: /* should never happen! */
							send_log(LOG_DEBUG,"Error: unrecognized pattern type: `%%%c'\n", *fmt);
							exit(1); // FIXME
					}

					if (buf-start <= 0)
						return 3;
				}
				fmt++;
				break;
				
			case ' ':
			case '\t':
			case '\n':
			case '\r':
			case '\f':
			case '\v':
				/* don't match if not at least one space */
				if(!isspace(*buf))
					return 0;
				else
					buf++;

				/* if next char in the form isn't a "space" pattern harvest remaining spaces */
				switch(*(fmt+1))
				{
					case ' ':
					case '\t':
					case '\n':
					case '\r':
					case '\f':
					case '\v':
						break;
					default:
						while (isspace(*buf))
							buf++;
				}
				fmt++;
				break;
				
			default:
				if (*fmt != *buf)
					return 1;
				fmt++; buf++;
		}
	}
	
	return 0;
}

/* Returns number of values scan will return */
int scan_count(const char *fmt)
{
	int count = 0;
	while (*fmt) {
		if (*fmt == '%') {
			if (*(++fmt) == '*')
				continue;

			while (isdigit(*fmt))
				fmt++;

			if (*fmt == 'd' || *fmt == 'i' || *fmt == 'u' ||
			    *fmt == 'f' ||
		 	    *fmt == 'o' || *fmt == 'x' || *fmt == 'X' ||
			    *fmt == 'c' || *fmt == 's') {
				count++;
			} else {
				send_log(LOG_DEBUG,"Error: unrecognized pattern type: `%%%c'\n", *fmt);
				exit(1); // FIXME
			}
		}
			
		fmt++;
	}
	return count;
}

#ifdef HAVE_LIBPCRE
int regscan(const char *bp, const struct match_t *match)
{
	int i;
	
	int rc = pcre_exec(match->regex.code, match->regex.extra, bp, strlen(bp), 0, 0, match->regex.ovector, (match->count+1) * 3);

	if (rc == PCRE_ERROR_NOMATCH) {
		return 1; /* no match */
	} else if (rc < 0) {
		send_log(LOG_ERR, "Regex matching error %d!\n", rc);
		return 1;
	}
	
	if (rc == 0) {
		send_log(LOG_ERR, "Regex ovector too small!!! Should not be happening!\n");
		rc = match->count + 1;
	}
		
	for (i = 1; i < rc; i++) {
		const char *start = bp + match->regex.ovector[2*i];
		int length = match->regex.ovector[2*i+1] - match->regex.ovector[2*i];
		
		match->counter[i-1]->value = strndup(start, length);
	}
	return 0;
}
#endif /* HAVE_LIBPCRE */
