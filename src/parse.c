/* parse.c - configuration file parser. */
/*
 *  RRDcollect  --  Round-Robin Database Collecting Daemon.
 *  Copyright (C) 2002, 2003  Dawid Kuroczko <qnex@knm.org.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "rrdcollect.h"

/*
 * Function dealing with cutting off the '#' comments...
 */
char buf[8192];

static int cut_comments(char *line)
{
	int closing;
	char *space = NULL; /* Space, the Final Fronteer. */
	
	for (; *line; line++) {
		switch (*line) {
			/* sorry, no multiline entries :-( */
			/* ignore next character */
			case '\\':
				if (!*(++line))
					return 0;
				break;
				
			/* patterns, ignore contents */
			case '/':
			case '"':
				closing = *line;
				for (; *line && *line != closing; line++);
				if (!*line) {
					send_log(LOG_ERR, "Error, umatched `%c'\n", closing);
					exit(1);
					return 1; /* error, unmatched "*" */
				}
						  
				break;

			/* isspace (sort of...) */
			case ' ':
			case '\f':
			case '\t':
			case '\v':
				if (!space)
					space = line;
				break;
				
			case '#':
			case '\0':
			case '\n':
			case '\r':
				if (space)
					*space = '\0';
				*line = '\0';
				return 0;

			/* remaining characters */
			default:
				space = NULL;
				break;
		}
	}
	return 0;
}


/* 
 * Functions for getting config variables in
 *     foo = bar
 * style.
 */

static char *skip_config_name(char *line)
{
	while (isalnum(*line) || *line == '_')
		line++;
	if (!*line)
		return NULL;
	return line;
}

static const char *skip_equal_sign(const char *line)
{
	/* skip spaces */
	while (isspace(*line))
		line++;

	/* find equal sign */
	if (*(line++) != '=')
		return NULL;

	/* skip more spaces */
	while (isspace(*line))
		line++;

	if (!*line)
		return NULL;
	return line;
}

static int get_config_var(char *name)
{
	const char *value;
	char *div;

	if (!(div = skip_config_name(name)))
		return 1; /* if it ends there, it's not config var */
	
	if (!(value = skip_equal_sign(div)))
		return 2; /* no value, no config */
	
	/* now we can modify the line */
	*div = '\0';

	/* and find and set those values */
	set_config_variable(name, value);

	return 0;
}


/*
 * Function for getting uri domain information in
 *     file://host/path/to/file
 * style.
 */

static char *find_domain_end(char *line)
{
	while (isalpha(*line))
		line++;

	if (line[0] == ':' && line[1] == '/' && line[2] == '/')
		return line;
	
	return NULL;
}

static char *find_host_end(char *line)
{
	for (;*line != '/'; line++) {
		if (!isgraph(*line))
			return NULL;
	}
	return line;
}

static int get_uri(char *domain, struct uri_t *uri)
{
	char *dend, *host, *path;
       
	if (!(dend = find_domain_end(domain)))
		return 1;

	host = dend+2;

	if (!(path = find_host_end(host)))
		return 2;

	/* FIXME: smarted domain handling!!! */
	if (strncmp(domain, "file", dend-domain)
#ifdef ENABLE_EXEC
				&& strncmp(domain, "exec", dend-domain)
#endif /* ENABLE_EXEC*/
			) {
		return 3;
	}

	/* Now we can modify the line */
	*(path++) = '\0';
	*dend = '\0';

#ifdef ENABLE_EXEC
	if(strcmp(domain,"exec")==0) {
		if(path[0]!='/') {
			send_log(LOG_ERR,"Path must start with /");
			exit(1);
		}
	}
#endif /* ENABLE_EXEC*/

	/* FIXME: checking whether file isgraph() all the way */

	uri->domain = strdup(domain);
	uri->host = strdup(host);
	uri->path = strdup(path);
	
	return 0;
}

/*
 * Function for parsing patterns in config file, like:
 *     4:"cpu %d" cpu.rrd:uptime
 *     5:/foo bar (\d+)/ cpu.rrd:stat
 */
static char *find_end_of_pattern(char *line, int c)
{
	for (;*line; line++) {
		if (*line == '\\') {
			if (!*(++line))
				return NULL;
		}
			
		if (*line == c)
			return line;
	}
	return NULL;
}

#ifndef PCRE_INFO_CAPTURECOUNT
static int count_parenthesis(const char *pat)
{
	int count = 0;
	while (*pat) {
		if (*pat == '\\') {
			if (*(++pat) == '\0')
				return count;
		}
		if (*pat == '(')
			count++;
		pat++;
	}
	return count;
}
#endif /* PCRE_INFO_CAPTURECOUNT */

static int get_pattern(char *pat, struct test_t *test)
{
	struct match_t match;

	match.line = 0;

	/* pat number prefix */
	if (isdigit(*pat)) {
		match.line = atoi(pat);
		while (isdigit(*(++pat)))
			;
		if (*(pat++) != ':')
			return 1; /* should be colon preceded */
		
		/* skip blanks, if any */
		while (isspace(*pat))
			pat++;
	}

	if (*pat == '"' || *pat == '/') {
		int type = *pat;
		char *eol = find_end_of_pattern(++pat, type);
		char *store;
		char *rrdfile = NULL;
		int i;
		
		if (!eol)
			return 2; /* erratic pattern */

		for (store = eol+1; isspace(*store); store++)
			;

		*eol = '\0';

		/* 
		 * FIXME: we should check the syntax here, not just copy as is,
		 *        got to fix it some day...
		 *        
		 * FIXME: Especially, we should take care and free allocated
		 *        stuff.  I guess I'm too lazy and I trust kernel too
		 *        much. ;-)
		 */
		if (type == '"') {
			match.type = MATCH_PATTERN;
			match.pattern = strdup(pat);
			match.count = scan_count(pat);
		} else {
#ifdef HAVE_LIBPCRE
			const char *error;
			int n;
			
			match.type = MATCH_REGEX;
			match.regex.code = pcre_compile(pat, 0 /* PCRE_CASELESS */, &error, &n, NULL);
			if (!match.regex.code) {
				send_log(LOG_ERR, "PCRE compilation failed at offset %d: %s\nPattern being compiled was: /%s/\n", n, error, pat);
				exit(1);
			}

			match.regex.extra = pcre_study(match.regex.code, 0, &error);
			if (error) {
				send_log(LOG_ERR, "PCRE studying failed: %s\nPattern being studied was: /%s/\n", error, pat);
				exit(1);
			}

# ifdef PCRE_INFO_CAPTURECOUNT
			pcre_fullinfo(match.regex.code, match.regex.extra, PCRE_INFO_CAPTURECOUNT, &n);
# else
#  warning "Consider getting newer libpcre.  This one doesn't have PCRE_INFO_CAPTURECOUNT."
			n = count_parenthesis(pat);
# endif /* PCRE_INFO_CAPTURECOUNT */
			match.regex.ovector = malloc(sizeof(int) * (n+1) * 3);
			match.count = n;
#else
			send_log(LOG_ERR, "This version is not compiled with libpcre support.\n");
			send_log(LOG_ERR, "Please make sure libpcre is installed and recompile.\n");
			exit(1);
#endif /* HAVE_LIBPCRE */
		}
		match.counter = malloc(match.count * sizeof(struct counter_t *));

		for (i = 0; i < match.count;) {
			char *start = store;

			while (*store != ':' && *store != ',' && isgraph(*store))
				store++;
			if (*store == ':') {
				*(store++) = '\0';
				rrdfile = start;
				continue; /* instead of else; looks nicer */
			}

			if (!rrdfile) {
				send_log(LOG_ERR, "RRD file name must be given first; in pattern \"%s\".\n", pat);
				exit(1);
			}
			
			if (*store)
				*(store++) = '\0';
			
			match.counter[i++] = add_rrd_counter(rrdfile, start);

			if (!*store && i < match.count) {
				send_log(LOG_ERR, "Not enough targets in pattern \"%s\"!!!\n", pat);
				exit(1);
			}
		}
		if (*store) {
			send_log(LOG_ERR, "Too many targets in pattern \"%s\"!!!\n", pat);
			exit(1);
		}

		i = test->count++;
		test->match = realloc(test->match, test->count * sizeof(struct match_t *));
		test->match[i] = malloc(sizeof(struct match_t));
		memcpy(test->match[i], &match, sizeof(struct match_t));

		return 0;
	}
	return 3;
}

/*
 * Config file parsing engine.
 */

/* TODO: Use yacc, maybe?... */

int parse_conf(const char *conf_file)
{
	FILE *conf = fopen(conf_file, "r");

	struct uri_t uri;
	struct test_t *test = NULL;

	if (!conf)
		return 1;
	
	while (fgets(buf, sizeof(buf), conf)) {
		char *bp = buf;
		
		while (isspace(*bp))
			bp++; /* skip spaces */
		cut_comments(buf);

		if (*bp == '\0')
			continue; /* blank line */

		if (!get_config_var(bp))
			continue; /* a config variable */

		if (!get_uri(bp, &uri)) {
			test = add_test_uri(&uri);
			continue; /* an URI domain specifier */
		}

		/* TODO: make it domain specific */
		if (!get_pattern(bp, test)) {
			continue; /* a pattern */
		}

		/* Something else?  Blame it on user and do a se-puku! */
		send_log(LOG_ERR, "Unrecognized config line:\n%s\n", buf);
		fclose(conf);
		exit(1);
	}

	fclose(conf);
	return 0;
}
