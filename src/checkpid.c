/* checkpid.c - check if we are not running the another copy */
/*
 *  RRDcollect  --  Round-Robin Database Collecting Daemon.
 *  Copyright (C) 2009  Artur R. Czechowski <arturcz@hell.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "rrdcollect.h"
#include <fcntl.h>
#include <sys/file.h>

/* lockfile in rrdcollect.pid */
/* five digits+"\n\0" */
#define PID_LENGTH 7

const char *pid_file=PIDFILE;
static int pidfd=-1;

/* checks /var/run/rrdcollect.pid if another copy of daemon is running */
/*
  Algorithm:
  if file does not exist create and open it
  else open it;
  remember the file descriptor
  try to lock the file
  if lock is successfull we are OK
  if lock failed we assume there is another copy

*/
int is_another_copy(void) {
	int res;

#ifdef O_CLOEXEC
	pidfd=open(pid_file,
		O_RDWR|O_CREAT|O_CLOEXEC|O_SYNC,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH); /* mode 644 */
#else
	pidfd=open(pid_file,
		O_RDWR|O_CREAT|O_SYNC,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH); /* mode 644 */
#endif
	if(pidfd==-1) {
		send_log(LOG_ERR,"Cannot open PID file %s. Error [%d] %s\n",
			pid_file,errno,strerror(errno));
		exit(1);
	}

#ifndef O_CLOEXEC
	res=fcntl(pidfd,F_SETFD,FD_CLOEXEC);
	if(res==-1) {
		send_log(LOG_ERR,"Cannot set FD_CLOEXEC flag on PID file %s. "
				 "Error [%d] %s\n",
			 pid_file,errno,strerror(errno));
	}
#endif

	send_log(LOG_DEBUG,"Opened PID file, fd=%d\n",pidfd);

	res=flock(pidfd, LOCK_EX|LOCK_NB);
	send_log(LOG_DEBUG,"Trying to lock the pidfile with result %d\n",res);
	if(res==-1) {
		send_log(LOG_DEBUG,"Extended result is [%d] %s\n",
			errno,strerror(errno));
		if(errno==EWOULDBLOCK) {
			send_log(LOG_DEBUG,"EWOULDBLOCK - other copy is running");
			return 1;
		}
		send_log(LOG_ERR,"Cannot lock PID file %s. Error [%d] %s\n",
			pid_file,errno,strerror(errno));
		exit(1);
	}
	send_log(LOG_DEBUG,"We can run!\n");

	/* At the moment we have exclusive access to the file,
	   that means no other copy is running */
	return 0;
}

void write_pidfile(void) {
	char pid_string[PID_LENGTH];
	int res;
	if(pidfd==-1) {
		send_log(LOG_ERR,"PID file is unavailable for some reason. Please see syslog for hints. If still in doubt please send us a bugreport.\n");
		exit(1);
	}
	if(snprintf(pid_string,PID_LENGTH,"%i\n",getpid())==-1) {
		send_log(LOG_ERR,"PID_LENGTH should be increased, please, send us a bugreport.\n");
		exit(1);
	}
	send_log(LOG_DEBUG,"Write PID %s(%d) to file fd=%d\n",pid_string,strlen(pid_string),pidfd);
	res=write(pidfd,(void*)pid_string,strlen(pid_string));
	if(res==-1) {
		send_log(LOG_ERR,"Cannot write to PID file [%d] %s\n",
			errno,strerror(errno));
		exit(1);
	}
}

void release_pidfile() {
	close(pidfd);
	unlink(PIDFILE); /* FIXME-ac error handling */
}
